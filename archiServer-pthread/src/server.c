/*
 * server.c
 *
 *  Created on: Feb 4, 2015
 *      Author: still
 */

#define _XOPEN_SOURCE 500

#include <pthread.h>
#include <semaphore.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <stdbool.h>
#include <signal.h>
#include <time.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include "src/common.h"
#include "server.h"

void * creerProcessusReservation(void * requete);
void setPlacesDiponiblesFusee(int indiceJour, int indiceFusee, int placesDisponibles);
int reserver(requeteReservation requete, reponseReservation * reponse);
int getPlacesDisponiblesFusee(int indiceJour, int indiceFusee);
int getPlacesDisponiblesJour(int jour);
int consulter(requeteConsultation requete, reponseConsultation * reponse);
void dettacherSHM();
void supprimerSHM(int shmid);
void attacherSHM(int shmid);
void creerSHM(int * shmid);
void supprimerMSQ(int msqid);
void creerMSQ(int * msqid);
void arreterServeur(int msqid, int shmid);
void sig_handler(int signo);
void afficherMenuServeur();
void creerPlanning();
void shmgets(int * shms);
void shmarretserveur(int * shmidarrserv);

bool demandeArretServeur;
int nombreJoursPlanning = 10;
jour * jours;
int msqid;
int shmid, shmarrserv, shms;
sem_t * s;

int main(int argc, char **argv) {
	pid_t consultationPID, reservationPID;

	shmgets(&shms);

	creerMSQ(&msqid);
	creerSHM(&shmid);
	attacherSHM(shmid);
	creerPlanning();

	shmarretserveur(&shmarrserv);
	if ((demandeArretServeur = shmat(shmid, NULL, 0)) == (-1)) {
		perror("shmat arr serv");
		exit(EXIT_FAILURE);
	}
	demandeArretServeur = false;

	if ((consultationPID = fork()) == 0) {
		// Processus Consultation
		for (;;) {
			if (demandeArretServeur) {
				dettacherSHM();
				shmdt(&demandeArretServeur);
				exit(EXIT_SUCCESS);
			}
			else {
				if (ecouterFileMessage(msqid, getConsultationType())) {
					requeteConsultation requete;
					if (recevoirRequeteConsultation(msqid, &requete)) {
						reponseConsultation reponse;
						consulter(requete, &reponse);
						envoyerReponseConsultation(msqid, &reponse);
					}
				}
			}
		}
	}
	else if (consultationPID < (pid_t) 0) {
		perror("fork consultation");
		exit(EXIT_FAILURE);
	}
	else {
		if ((reservationPID = fork()) == 0) {
			// Processus Reservation
			pthread_t thread;
			if ((s = shmat(shms, NULL, 0)) == (sem_t *) (-1)) {
				perror("shmat sema");
				exit(EXIT_FAILURE);
			}

			for (int i = 0; i < nombreJoursPlanning; ++i) {
				sem_init(&s[i], 0, 1);
			}

			for (;;) {
				if (demandeArretServeur) {
					dettacherSHM();
					shmdt(&demandeArretServeur);
					shmdt(&s);
					wait(NULL);
					exit(EXIT_SUCCESS);
				}
				else {
					if (ecouterFileMessage(msqid, getReservationType())) {
						requeteReservation requete;
						if (recevoirRequeteReservation(msqid, &requete)) {
							sem_wait(&s[(requete.jour - 1)]);
							pthread_create(&thread, NULL, creerProcessusReservation, (void *) &requete);
							sem_post(&s[(requete.jour - 1)]);
						}
					}
				}
			}
		}
		else if (reservationPID < (pid_t) 0) {
			perror("fork reservation");
			exit(EXIT_FAILURE);
		}
		else {
			// Processus Serveur
			afficherMenuServeur();
			signal(SIGINT, sig_handler);

			for (;;) {
				if (demandeArretServeur) {
					shmdt(&demandeArretServeur);
					wait(NULL);
					arreterServeur(msqid, shmid);
				}
			}
		}
	}
}

void * creerProcessusReservation(void * requete) {
	requeteReservation * req = (requeteReservation *) requete;
	reponseReservation reponse;
	reserver(* req, &reponse);
	envoyerReponseReservation(msqid, &reponse);
	pthread_exit(NULL);
}

void setPlacesDiponiblesFusee(int indiceJour, int indiceFusee, int placesDisponibles) {
	jours[indiceJour].fusees[indiceFusee].places = placesDisponibles;
}

int reserver(requeteReservation requete, reponseReservation * reponse) {
	int jour = requete.jour;
	int placesDemandees = requete.places;
	reponse->mtype = requete.clientPID;

	if(jour > nombreJoursPlanning) {
		reponse->etat = (-1);
		return 1;
	}

	if (getPlacesDisponiblesJour(jour) < placesDemandees) {
		reponse->etat = 0;
		return 1;
	}

	int compteurF = 0;
	int indiceJour = (jour - 1);
	while (placesDemandees > 0) {
		int placesFusee;
		if ((placesFusee = getPlacesDisponiblesFusee(indiceJour, compteurF)) < placesDemandees) {
			setPlacesDiponiblesFusee(indiceJour, compteurF, 0);
			reponse->places[compteurF] = placesFusee;
			placesDemandees -= placesFusee;
		}
		else {
			setPlacesDiponiblesFusee(indiceJour, compteurF, getPlacesDisponiblesFusee(indiceJour, compteurF) - placesDemandees);
			reponse->places[compteurF] = placesDemandees;
			placesDemandees = 0;
		}

		compteurF++;
	}

	reponse->etat = compteurF;
	return 1;
}

int getPlacesDisponiblesFusee(int indiceJour, int indiceFusee) {
	return jours[indiceJour].fusees[indiceFusee].places;
}

int getPlacesDisponiblesJour(int jour) {
	if (jour > nombreJoursPlanning || jour < 1) {
		return (-1);
	}

	int placesDisponibles = 0;

	for (int f = 0; f < 4; ++f) {
		placesDisponibles += getPlacesDisponiblesFusee((jour - 1), f);
	}

	return placesDisponibles;
}

int consulter(requeteConsultation requete, reponseConsultation * reponse) {
	int jour = requete.jour;
	int placesDisponibles;
	reponse->mtype = requete.clientPID;
	reponse->places = (-1);

	if(jour > nombreJoursPlanning) {
		reponse->etat = (-1);
		return 1;
	}

	if ((placesDisponibles = getPlacesDisponiblesJour(jour)) == (-1)) {
		return 0;
	}

	reponse->etat = 1;
	reponse->places = placesDisponibles;
	return 1;
}

void dettacherSHM() {
	shmdt(jours);
}

void supprimerSHM(int shmid) {
	shmctl(shmid, IPC_RMID, 0);
}

void attacherSHM(int shmid) {
	if ((jours = shmat(shmid, NULL, 0)) == (jour *) (-1)) {
		perror("shmat");
		exit(EXIT_FAILURE);
	}
}

void creerSHM(int * shmid) {
	key_t shmkey = getSHMKEY();

	if ((* shmid = shmget(shmkey, 0, 0)) != (-1)) {
		shmctl(* shmid, IPC_RMID, 0);
	}

	int size = nombreJoursPlanning * sizeof(jour);
	if ((* shmid = shmget(shmkey, size, IPC_CREAT | IPC_EXCL | 0600)) == (-1)) {
		perror("shmget");
		exit(EXIT_FAILURE);
	}
}

void supprimerMSQ(int msqid) {
	msgctl(msqid, IPC_RMID, 0);
}

void creerMSQ(int * msqid) {
	key_t msqkey = getMSQKEY();

	if ((* msqid = msgget(msqkey, 0)) != (-1)) {
		msgctl(* msqid, IPC_RMID, 0);
	}

	if ((* msqid = msgget(msqkey, IPC_CREAT | IPC_EXCL | 0600)) == (-1)) {
		perror("msgget");
		exit(EXIT_FAILURE);
	}
}

void arreterServeur(int msqid, int shmid) {
	supprimerSHM(shmid);
	shmctl(shmarrserv, IPC_RMID, 0);
	supprimerMSQ(msqid);
	exit(EXIT_SUCCESS);
}

void sig_handler(int signo) {
	if (signo == SIGINT) {
		demandeArretServeur = true;
	}
}

void afficherMenuServeur() {
	effacerEcran();
	printf("Appuyer sur Ctrl + C pour arrêter le serveur...\n");
	fflush(stdout);
}

void shmgets(int * shms) {
	key_t shmkey = 15;

	if ((* shms = shmget(shmkey, 0, 0)) != (-1)) {
		shmctl(* shms, IPC_RMID, 0);
	}

	int size = sizeof(sem_t) * nombreJoursPlanning;
	if ((* shms = shmget(shmkey, size, IPC_CREAT | IPC_EXCL | 0600)) == (-1)) {
		perror("shmget");
		exit(EXIT_FAILURE);
	}
}

void shmarretserveur(int * shmidarrserv) {
	key_t shmkey = 30;

	if ((* shmidarrserv = shmget(shmkey, 0, 0)) != (-1)) {
		shmctl(* shmidarrserv, IPC_RMID, 0);
	}

	int size = sizeof(bool);
	if ((* shmidarrserv = shmget(shmkey, size, IPC_CREAT | IPC_EXCL | 0600)) == (-1)) {
		perror("shmget");
		exit(EXIT_FAILURE);
	}
}

void creerPlanning() {
	srand((unsigned) time(NULL));

	for (int j = 0; j < nombreJoursPlanning; ++j) {
		for (int f = 0; f < 4; ++f) {
			jours[j].fusees[f].places = (rand() % 9) + 2;
		}
	}
}
