/*
 * common.h
 *
 *  Created on: Feb 5, 2015
 *      Author: still
 */

#ifndef COMMON_H_
#define COMMON_H_

#define MSQKEY 16
#define SHMKEY 32
#define CONSULTATION 1
#define RESERVATION 2

#include <sys/ipc.h>

typedef struct {
	long mtype;
	int places[4];
	int etat;
} reponseReservation;

typedef struct {
	long mtype;
	int places;
	int etat;
} reponseConsultation;

typedef struct {
	long mtype;
	int jour;
	int places;
	pid_t clientPID;
} requeteReservation;

typedef struct {
	long mtype;
	int jour;
	pid_t clientPID;
} requeteConsultation;

extern int getReservationType();
extern int getConsultationType();
extern int envoyerReponseReservation(int msqid, reponseReservation * reponse);
extern int recevoirReponseReservation(int msqid, reponseReservation * reponse, pid_t pid);
extern int recevoirRequeteReservation(int msqid, requeteReservation * requete);
extern int envoyerRequeteReservation(int msqid, requeteReservation * requete);
extern int envoyerReponseConsultation(int msqid, reponseConsultation * reponse);
extern int recevoirReponseConsultation(int msqid, reponseConsultation * reponse, pid_t pid);
extern int recevoirRequeteConsultation(int msqid, requeteConsultation * requete);
extern int ecouterFileMessage(int msqid, long mtype);
extern int envoyerRequeteConsultation(int msqid, requeteConsultation * requete);
key_t getSHMKEY();
key_t getMSQKEY();
extern int ecrireSurLaPipe(int * filedes);
extern int lireSurLaPipe(int * filedes);
void fermerPipeEnEcriture(int * filedes);
void fermerPipeEnLecture(int * filedes);
void utiliserPipeEnEcriture(int * filedes);
void utiliserPipeEnLecture(int * filedes);
extern int recevoirEntier(int * entier, int taille);
void attendreEntreeUtilisateur();
void effacerEcran();

#endif /* COMMON_H_ */
