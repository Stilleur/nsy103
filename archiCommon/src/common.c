/*
 * common.c
 *
 *  Created on: Feb 5, 2015
 *      Author: still
 */

#define _XOPEN_SOURCE 500

#include <errno.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "common.h"

extern int getReservationType() {
	return RESERVATION;
}

extern int getConsultationType() {
	return CONSULTATION;
}

extern int recevoirReponseReservation(int msqid, reponseReservation * reponse, pid_t pid) {
	int result;
	int length = sizeof(reponseReservation) - sizeof(long);
	if ((result = msgrcv(msqid, reponse, length, (long) pid, 0)) == (-1)) {
		return 0;
	}

	return 1;
}

extern int envoyerReponseReservation(int msqid, reponseReservation * reponse) {
	int result;
	int length = sizeof(reponseReservation) - sizeof(long);
	if ((result = msgsnd(msqid, reponse, length, 0)) == (-1)) {
		return 0;
	}

	return 1;
}

extern int recevoirRequeteReservation(int msqid, requeteReservation * requete) {
	int result;
	int length = sizeof(requeteReservation) - sizeof(long);
	if ((result = msgrcv(msqid, requete, length, getReservationType(), 0)) == (-1)) {
		return 0;
	}

	return 1;
}

extern int envoyerRequeteReservation(int msqid, requeteReservation * requete) {
	int result;
	int length = sizeof(requeteReservation) - sizeof(long);
	if ((result = msgsnd(msqid, requete, length, 0)) == (-1)) {
		return 0;
	}

	return 1;
}

extern int envoyerReponseConsultation(int msqid, reponseConsultation * reponse) {
	int result;
	int length = sizeof(reponseConsultation) - sizeof(long);
	if ((result = msgsnd(msqid, reponse, length, 0)) == (-1)) {
		return 0;
	}

	return 1;
}

extern int recevoirReponseConsultation(int msqid, reponseConsultation * reponse, pid_t pid) {
	int result;
	int length = sizeof(reponseConsultation) - sizeof(long);
	if ((result = msgrcv(msqid, reponse, length, (long) pid, 0)) == (-1)) {
		return 0;
	}

	return 1;
}

extern int recevoirRequeteConsultation(int msqid, requeteConsultation * requete) {
	int result;
	int length = sizeof(requeteConsultation) - sizeof(long);
	if ((result = msgrcv(msqid, requete, length, getConsultationType(), 0)) == (-1)) {
		return 0;
	}

	return 1;
}

extern int ecouterFileMessage(int msqid, long mtype) {
	int result;
	if ((result = msgrcv(msqid, NULL, 0, mtype, IPC_NOWAIT)) == (-1)) {
		if (errno == E2BIG) {
			return 1;
		}
	}

	return 0;
}

extern int envoyerRequeteConsultation(int msqid, requeteConsultation * requete) {
	int result;
	int length = sizeof(requeteConsultation) - sizeof(long);
	if ((result = msgsnd(msqid, requete, length, 0)) == (-1)) {
		return 0;
	}

	return 1;
}

key_t getSHMKEY() {
	return SHMKEY;
}

key_t getMSQKEY() {
	return MSQKEY;
}

extern int ecrireSurLaPipe(int * filedes) {
	return filedes[1];
}

extern int lireSurLaPipe(int * filedes) {
	return filedes[0];
}

void utiliserPipeEnEcriture(int * filedes) {
	fermerPipeEnLecture(filedes);
}

void utiliserPipeEnLecture(int * filedes) {
	fermerPipeEnEcriture(filedes);
}

void fermerPipeEnEcriture(int * filedes) {
	close(filedes[1]);
}

void fermerPipeEnLecture(int * filedes) {
	close(filedes[0]);
}

extern int recevoirEntier(int * entier, int taille) {
	char * input = malloc(taille * sizeof(char));
	char * fgetsResult = malloc(taille * sizeof(char));
	if ((fgetsResult = fgets(input, sizeof(input), stdin)) == NULL)
		return 0;

	int sscanfResult;
	if ((sscanfResult = sscanf(input, "%d", entier)) <= 0)
		return 0;

	return 1;
}

void attendreEntreeUtilisateur() {
	printf("Appuyer sur un touche pour continuer...");
	fflush(stdout);
	getchar();
}

void effacerEcran() {
	printf("\033c");
}
