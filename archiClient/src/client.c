/*
 * client.c
 *
 *  Created on: Feb 4, 2015
 *      Author: still
 */

#define _XOPEN_SOURCE 500

#include <stdbool.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "src/common.h"
#include "client.h"

void afficherReponseReservation(reponseReservation reponse, int jour);
void afficherReponseConsultation(reponseConsultation reponse, int jour);
void seConnecter();
void afficherMenuReservationJour(int places);
void afficherMenuReservationPlaces();
void menuReservation();
void afficherMenuConsultation();
void menuConsultation();
void afficherMenuPrincipal();

int msqid;
bool estConnecte = false;

int main(int argc, char **argv) {
	effacerEcran();
	for (;;) {
		if (estConnecte) {
			int choixMenu;
			int result;
			do {
				afficherMenuPrincipal();
			} while ((result = recevoirEntier(&choixMenu, 2)) == 0);

			switch (choixMenu) {
			case 1:
				menuConsultation();
				break;
			case 2:
				menuReservation();
				break;
			case 0:
				exit(EXIT_SUCCESS);
			}
		}
		else {
			seConnecter();
		}
	}
}

void seConnecter() {
	int msqkey = getMSQKEY();
	int beat = 5;
	int clean = 0;
	while ((msqid = msgget(msqkey, 0)) == (-1)) {
		if (beat == 5) {
			if (clean == 5) {
				effacerEcran();
				clean = 0;
			}
			printf("Demarrer le serveur...\n");
			beat = 0;
			clean++;
		}
		beat++;
		sleep(1);
	}

	estConnecte = true;
}

void afficherMenuReservationJour(int places) {
	afficherMenuReservationPlaces();
	printf("%d\n", places);
	printf("Sur quel jour souhaitez-vous reserver ? ");
}

void afficherMenuReservationPlaces() {
	effacerEcran();
	printf("Menu Reservation\n\n");
	printf("Combien de place(s) souhaitez-vous reserver ? ");
}

void afficherReponseReservation(reponseReservation reponse, int jour) {
	printf("\n\n");
	if (reponse.etat > 0) {
		printf("Votre reservation pour le jour %d :\n", jour);
		for (int i = 0; i < reponse.etat; ++i) {
			printf("\t%d place(s) sur la fusee %d\n", reponse.places[i], (i + 1));
		}
	}
	else if (reponse.etat == 0) {
		printf("Il n'y a pas assez de place(s) sur le jour %d pour faire la reservation\n", jour);
	}
	else if (reponse.etat == (-1)) {
		printf("Le jour %d saisi ne fait pas partie du planning (%d est trop grand)\n", jour, jour);
	}
	else {
		printf("Une erreur s'est produite lors de la reservation\n");
	}
}

void menuReservation() {
	int placesReservation;
	int result;
	do {
		afficherMenuReservationPlaces();
	} while ((result = recevoirEntier(&placesReservation, 2)) == 0);

	int jourReservation;
	do {
		afficherMenuReservationJour(placesReservation);
	} while ((result = recevoirEntier(&jourReservation, 2)) == 0);
	requeteReservation requete;
	requete.mtype = getReservationType();
	requete.jour = jourReservation;
	requete.places = placesReservation;
	requete.clientPID = getpid();
	if (envoyerRequeteReservation(msqid, &requete)) {
		reponseReservation reponse;
		if (recevoirReponseReservation(msqid, &reponse, requete.clientPID)) {
			afficherReponseReservation(reponse, requete.jour);
			attendreEntreeUtilisateur();
		}
		else {
			estConnecte = false;
		}
	}
	else {
		estConnecte = false;
	}
}

void afficherMenuConsultation() {
	effacerEcran();
	printf("Menu Consultation\n\n");
	printf("Quel jour souhaitez-vous consulter ? ");
}

void afficherReponseConsultation(reponseConsultation reponse, int jour) {
	switch (reponse.etat) {
	case (-1):
					printf("Le jour %d saisi ne fait pas partie du planning (%d est trop grand)\n", jour, jour);
	break;
	case 1:
		printf("\n\nIl y a %d places disponibles pour le jour %d\n", reponse.places, jour);
		break;
	}
}

void menuConsultation() {
	int jourConsultation;
	int result;
	do {
		afficherMenuConsultation();
	} while ((result = recevoirEntier(&jourConsultation, 2)) == 0);
	requeteConsultation requete;
	requete.mtype = getConsultationType();
	requete.jour = jourConsultation;
	requete.clientPID = getpid();
	if (envoyerRequeteConsultation(msqid, &requete)) {
		reponseConsultation reponse;
		if (recevoirReponseConsultation(msqid, &reponse, requete.clientPID)) {
			afficherReponseConsultation(reponse, requete.jour);
			attendreEntreeUtilisateur();
		}
		else {
			estConnecte = false;
			printf("Le client a ete deconnecte\n");
			attendreEntreeUtilisateur();
		}
	}
	else {
		estConnecte = false;
		printf("Le client a ete deconnecte\n");
		attendreEntreeUtilisateur();
	}
}

void afficherMenuPrincipal() {
	effacerEcran();
	printf("Menu Principal\n\n");
	printf("1. Consulter\n");
	printf("2. Reserver\n");
	printf("0. Quitter\n\n");
	printf("Votre choix : ");
}
